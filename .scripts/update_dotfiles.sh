#!/bin/sh

#dotfiles = /usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME

/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/BetterDiscord/themes/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/alacritty/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/awesome/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/cudatext/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/dunst/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/Element/config.json
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/fish/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add "$HOME/.config/gtk-2.0/"
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add "$HOME/.config/gtk-3.0/"
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/herbstluftwm/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/i3/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/kitty/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/nvim/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/picom/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/polybar/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/redshift.conf
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.config/starship.toml

/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.dmenu/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.doom.d/init.el
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.doom.d/config.el
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.doom.d/packages.el
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.icons/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.scripts/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.surf/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.themes/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.tmux/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.vscode/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.wallpapers/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add "$HOME/dmenu-5.0/"
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/st/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/surf/
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.bashrc
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add "$HOME/.gtkrc-2.0"
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.profile
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.spacemacs
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.spectrwm.conf
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.tmux.conf
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/.zshrc
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/LICENSE
/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME add $HOME/README.org
