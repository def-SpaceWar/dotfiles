#!/bin/bash
declare -a myList=(
"quit"
"new"
)

# TODO make this work, append the list to the `tmux ls` and make it so u can also add new sessions
choice=$(printf '%s\n' "$(tmux ls)" | rofi -dmenu -p "Tmux Launch")

id=$(echo $choice | awk -F ':' '{print $1}')
echo $id

st -e tmux attach-session -t $id
