#!/bin/bash

choice=$(rofi -dmenu -p "Set Brightness")

if [[ $choice > 0 ]]; then
    xrandr --output eDP-1 --brightness $choice
    echo "Setting brightness to $choice."

else
    echo "Nothing" && exit 1

fi
