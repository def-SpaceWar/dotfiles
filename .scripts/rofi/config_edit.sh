#!/bin/bash

declare -a myConfigs=(
"alacritty - $HOME/.config/alacritty/alacritty.yml"
"awesome - $HOME/.config/awesome/README.org"
"awesome theme.lua - $HOME/.config/awesome/themes/default/theme.lua"
"doom config.el - $HOME/.doom.d/config.el"
"doom init.el - $HOME/.doom.d/init.el"
"doom packages.el - $HOME/.doom.d/packages.el"
"dunstrc - $HOME/.config/dunst/dunstrc"
"fish - $HOME/.config/fish/config.fish"
"herbstluftwm - $HOME/.config/herbstluftwm/autostart"
"i3 - $HOME/.config/i3/config"
"kitty - $HOME/.config/kitty/kitty.conf"
"neovim - $HOME/.config/nvim/init.vim"
"polybar - $HOME/.config/polybar/config"
"redshift - $HOME/.config/redshift.conf"
"rofi config - $HOME/.config/rofi/config.rasi"
"rofi theme - $HOME/.config/rofi/my_theme.rasi"
"spectrwm - $HOME/.spectrwm.conf"
"starship - $HOME/.config/starship.toml"
"termonad - $HOME/.config/termonad/termonad.hs"
"xmobar - $HOME/.config/xmobar/xmobarrc"
"xmonad - $HOME/.xmonad/README.org"
"quit"
)

choice=$(printf '%s\n' "${myConfigs[@]}" | rofi -dmenu -p "Configs")

if [[ $choice == "quit" ]]; then
    echo "QUITTING" && exit 1

elif [[ $choice ]]; then
    config=$(printf '%s\n' "${choice}" | awk '{print $NF}')
    echo $config
    emacsclient -c "$config"

else
    echo "Nothing" && exit 1

fi
