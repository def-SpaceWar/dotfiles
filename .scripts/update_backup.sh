#!/bin/sh

#backup = /usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME

/usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME add $HOME/Code/
/usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME add $HOME/MinecraftStuff/
/usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME add $HOME/.local/share/multimc/instances/
/usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME add $HOME/org/
/usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME add $HOME/Pictures/
/usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME add $HOME/.config/mu4e/
