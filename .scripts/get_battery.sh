#!/bin/bash

battery=$(acpi | cut -d' ' -f 4 | cut -d% -f 1)
status=$(acpi | cut -d: -f 2,2 | cut -d, -f 1,1)

if [ $battery -lt 20 ]; then
    battery="<fc=$1>$battery</fc>"
elif [ $battery -lt 90 ]; then
    battery="<fc=$2>$battery</fc>"
else
    battery="<fc=$3>$battery</fc>"
fi

if [ $status = "Discharging" ]; then
    status="<fc=$1>$status</fc>"
elif [ $status = "Charging" ]; then
    status="<fc=$2>$status</fc>"
elif [ $status = "Full" ]; then
    status="<fc=$3>$status</fc>"
fi

echo "Battery: $battery$status"
