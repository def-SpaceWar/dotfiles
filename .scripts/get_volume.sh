#!/bin/bash

volume=$(amixer -D pulse sget Master | awk -F"[][]" '/Left:/ { print $2 }')
volume=$(echo "$volume" | rev | cut -c2- | rev)

muted=$(amixer sget Master | awk -F"[][]" '/Mono:/ { print $6 }')

if [ "$volume" -gt 100 ]; then
    # make it red
    volume="<fc=$1>$volume%</fc>"
elif [ "$volume" -gt 50 ]; then
    # make it yellow
    volume="<fc=$2>$volume%</fc>"
else
    # make it green
    volume="<fc=$3>$volume%</fc>"
fi

if [ "$muted" = "on" ]; then
    muted="<fc=$3> ON</fc>"
else
    muted="<fc=$1> MUTED</fc>"
fi

echo "Volume: $volume$muted"
