#!/bin/bash
set -euo pipefail

if [ $(pidof lxsession || echo 0) -eq 0 ]
then
    lxsession --session=awesome -a &
else
    echo "lxsession is on"
fi

if [ $(pidof picom || echo 0) -gt 0 ]
then
    echo "picom is on"
else
    picom &
fi

if [ $(pidof flameshot || echo 0) -gt 0 ]
then
    echo "flameshot is on"
else
    flameshot &
fi

if [ $(pidof Discord || echo 0) -eq 0 ]
then
    discord &
else
    echo "discord is on"
fi

if [ $(pidof element-desktop || echo 0) -eq 0 ]
then
    element-desktop &
else
    echo "element is on"
fi

if [ $(pidof redshift || echo 0) -eq 0 ]
then
    redshift-gtk &
else
    echo "redshift is on"
fi

if [ $(pidof emacs || echo 0) -eq 0 ]
then
    emacs --daemon &
else
    echo "emacs is on"
fi

if [ $(pidof nm-applet || echo 0) -eq 0 ]
then
    nm-applet &
else
    echo "nm-applet is on"
fi

# nitrogen --restore
feh --bg-scale $HOME/.wallpapers/monokai_apprentice_abstract.png
