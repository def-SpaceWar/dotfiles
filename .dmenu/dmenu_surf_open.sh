#!/bin/bash
declare -a myWebsites=(
"suckless - suckless.org"
"quit"
)

choice=$(printf '%s\n' "${myWebsites[@]}" | dmenu -i -l 20 -p "Web: ")

if [[ $choice == "quit" ]]; then
    echo "QUITTING" && exit 1

elif [[ $choice ]]; then
    website=$(printf '%s\n' "${choice}" | awk '{print $NF}')
    echo $config
    surf "$website"

else
    echo "Nothing" && exit 1

fi
