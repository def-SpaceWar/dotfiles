#!/bin/bash

declare -a myConfigs=(
"alacritty - $HOME/.config/alacritty/alacritty.yml"
"awesome - $HOME/.config/awesome/rc.lua"
"dmenu config.h - $HOME/dmenu-5.0/config.h"
"dunstrc - $HOME/.config/dunst/dunstrc"
"fish - $HOME/.config/fish/config.fish"
"herbstluftwm - $HOME/.config/herbstluftwm/autostart"
"i3 - $HOME/.config/i3/config"
"kitty - $HOME/.config/kitty/kitty.conf"
"neovim - $HOME/.config/nvim/init.vim"
"polybar - $HOME/.config/polybar/config"
"redshift - $HOME/.config/redshift/redshift.conf"
"spectrwm - $HOME/.spectrwm.conf"
"starship - $HOME/.config/starship.toml"
"quit"
)

choice=$(printf '%s\n' "${myConfigs[@]}" | dmenu -i -l 20 -p "Configs: ")

if [[ $choice == "quit" ]]; then
    echo "QUITTING" && exit 1

elif [[ $choice ]]; then
    config=$(printf '%s\n' "${choice}" | awk '{print $NF}')
    echo $config
    emacsclient -c "$config"

else
    echo "Nothing" && exit 1

fi
