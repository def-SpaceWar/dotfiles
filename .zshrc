#
# ~/.zshrc
#

PATH="$HOME/.bin:$PATH"
PATH="$HOME/.local/bin:$PATH"
PATH="$PATH:$HOME/.emacs.d/bin/"
[ -f "/home/aryan/.ghcup/env" ] && source "/home/aryan/.ghcup/env" # ghcup-env

autoload -U colors && colors
export PS1=$'\n'"%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}"$'\n'"$%b "

HISTSIZE=131072 # 2^17 cuz 2^16 < 100_000
SAVEHIST=131072
HISTFILE=$HOME/.zsh_history

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

export EDITOR='nvim'
export VISUAL='nvim'
export MANPAGER="nvim -c 'set ft=man' -"

ex ()
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1   ;;
            *.tar.gz)    tar xzf $1   ;;
            *.bz2)       bunzip2 $1   ;;
            *.rar)       unrar x $1   ;;
            *.gz)        gunzip $1    ;;
            *.tar)       tar xf $1    ;;
            *.tbz2)      tar xjf $1   ;;
            *.tgz)       tar xzf $1   ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1;;
            *.7z)        7z x $1      ;;
            *.deb)       ar x $1      ;;
            *.tar.xz)    tar xf $1    ;;
            *.tar.zst)   tar xf $1    ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
colorscript random
alias ls="exa -l"
alias la="exa -la"
alias lt="exa -ahT"
alias dotfiles="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"
alias backup="/usr/bin/git --git-dir=$HOME/backup --work-tree=$HOME"
alias rebuild="cd ~/termonad/; stack exec -- termonad & disown; exit"
alias killmc="killall java && exit"
alias sd="shutdown now"
alias tux="colorscript -e tux"

# Emacs Server Restart
esr ()
{
    killall emacs
    emacs --daemon & disown
}

# Restart Discord
dr () {
    killall -9 Discord
    discord & disown
    exit
}

eval "$(starship init zsh)"
source /home/aryan/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
