#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#0E0A17/g' \
         -e 's/rgb(100%,100%,100%)/#CFD5D6/g' \
    -e 's/rgb(50%,0%,0%)/#0E0A17/g' \
     -e 's/rgb(0%,50%,0%)/#7CB34F/g' \
 -e 's/rgb(0%,50.196078%,0%)/#7CB34F/g' \
     -e 's/rgb(50%,0%,50%)/#333232/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#333232/g' \
     -e 's/rgb(0%,0%,50%)/#CFD5D6/g' \
	"$@"
