#!/bin/sh
sed -i \
         -e 's/rgb(0%,0%,0%)/#302f27/g' \
         -e 's/rgb(100%,100%,100%)/#e7e6df/g' \
    -e 's/rgb(50%,0%,0%)/#22221b/g' \
     -e 's/rgb(0%,50%,0%)/#5f9182/g' \
 -e 's/rgb(0%,50.196078%,0%)/#5f9182/g' \
     -e 's/rgb(50%,0%,50%)/#22221b/g' \
 -e 's/rgb(50.196078%,0%,50.196078%)/#22221b/g' \
     -e 's/rgb(0%,0%,50%)/#f4f3ec/g' \
	"$@"
