#!/bin/sh
sed -i \
         -e 's/#282727/rgb(0%,0%,0%)/g' \
         -e 's/#c0c0c0/rgb(100%,100%,100%)/g' \
    -e 's/#282727/rgb(50%,0%,0%)/g' \
     -e 's/#5977D9/rgb(0%,50%,0%)/g' \
     -e 's/#393636/rgb(50%,0%,50%)/g' \
     -e 's/#c0c0c0/rgb(0%,0%,50%)/g' \
	"$@"
