<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>software that sucks less | suckless.org software that sucks less</title>
	<link rel="stylesheet" type="text/css" href="//suckless.org/pub/style.css"/>
</head>

<div id="header">
	<a href="//suckless.org/"><img src="//suckless.org/logo.svg" alt=""/></a>&nbsp;
	<a id="headerLink" href="//suckless.org/">suckless.org</a>
	<span class="hidden"> - </span>
	<span id="headerSubtitle">software that sucks less</span>
</div>
<hr class="hidden"/>
<div id="menu">
	<a href="//suckless.org/"><b>home</b></a>
	<a href="//dwm.suckless.org/">dwm</a>
	<a href="//st.suckless.org/">st</a>
	<a href="//core.suckless.org/">core</a>
	<a href="//surf.suckless.org/">surf</a>
	<a href="//tools.suckless.org/">tools</a>
	<a href="//libs.suckless.org/">libs</a>
	<a href="//ev.suckless.org/">e.V.</a>
	<span class="right">
		<a href="//dl.suckless.org">download</a>
		<a href="//git.suckless.org">source</a>
	</span>
</div>
<hr class="hidden"/>
<div id="content">
<div id="nav">
	<ul>
	<li><a href="/">about</a></li>
	<li><a href="//suckless.org/coding_style/">coding style/</a></li>
	<li><a href="//suckless.org/community/">community/</a></li>
	<li><a href="//suckless.org/conferences/">conferences/</a></li>
	<li><a href="//suckless.org/donations/">donations/</a></li>
	<li><a href="//suckless.org/faq/">faq/</a></li>
	<li><a href="//suckless.org/hacking/">hacking/</a></li>
	<li><a href="//suckless.org/other_projects/">other projects/</a></li>
	<li><a href="//suckless.org/people/">people/</a></li>
	<li><a href="//suckless.org/philosophy/">philosophy/</a></li>
	<li><a href="//suckless.org/project_ideas/">project ideas/</a></li>
	<li><a href="//suckless.org/rocks/">rocks/</a></li>
	<li><a href="//suckless.org/sucks/">sucks/</a></li>
	<li><a href="//suckless.org/wiki/"><b>wiki/</b></a></li>
	</ul>
</div>
<hr class="hidden"/>
<div id="main">

<h1>This wiki</h1>
<p>If you would like to contribute new content, you can clone this wiki to your
local host using the following command:</p>
<pre><code>git clone git://git.suckless.org/sites
</code></pre>
<p>or</p>
<pre><code>git clone https://git.suckless.org/sites
</code></pre>
<p>Then edit the wiki as you like. The markdown interpreter that is currently
used on suckless.org is
<a href="https://github.com/Gottox/smu">smu</a>.</p>
<p>For adding new files, after you created them, use:</p>
<pre><code>git add somefile
</code></pre>
<p>When you are finished, commit your changes with:</p>
<pre><code>git commit -a
</code></pre>
<p>There you enter some meaningful commit message and end the editor.</p>
<p>To push your changes to the queue for the review by the suckless moderators,
use:</p>
<pre><code>git push git://git.suckless.org/sites
</code></pre>
<p>The review of your changes might take a few days, due to the different
timezones we all live in.</p>
<p><strong>Please make sure to update for incoming changes using »git pull«, before you
push changes, to minimize merge problems.</strong></p>
<p>The wiki repository above is world-writable.</p>
<h2>Rules</h2>
<ul>
<li>If any abuse happens, we will disable world-writable access. Keep this in
mind! We kindly ask you to not destroy the way we like to collaborate with
the community.</li>
<li>Please do not add files bigger than <em>100kb</em>.</li>
<li>Please do not add unscaled large images. If needed provide a thumbnail.</li>
<li>Please do not add any binary files except screenshots or images related to
our software.</li>
<li>For datetimes use the international date format: yyyy-mm-dd.</li>
<li>The patches should be hosted in the repository itself. Providing an
additional mirror with the same content is OK.</li>
<li>Inline HTML, HTML files or inline JavaScript is not allowed and not supported.</li>
</ul>
<h2>Commit messages</h2>
<p>Try to provide a clear subject and a clear commit message.
The subject should not be more than 79 characters.</p>
<p>The format should be:</p>
<p>Subject not more than 79 characters&lt;newline&gt;
&lt;newline&gt;
Clear message describing the commit, line-wrapped to maximum of 79 characters.
This message can be optional for trivial commits.</p>
<h2>Markdown usage</h2>
<ul>
<li>The extension of newly created Markdown files must be <code>.md</code>.</li>
<li>There are some dialects of Markdown, please don't use too &quot;advanced&quot; features.</li>
</ul>
<h2>Review markdown</h2>
<p>A quick way to check what output will be generated on the site is to do:</p>
<pre><code>smu -n &lt; index.md | lynx -stdin
</code></pre>
<p>Please review your changes locally before pushing to avoid spamming the commit
log and the review process.</p>
<h2>Changes</h2>
<p>The incoming changes to the wiki are all sent to the wiki@
mailinglist. See <a href="//suckless.org/community">community</a> for how to
subscribe to this list.</p>
<h2>Moderators</h2>
<p>If you are a moderator, you will likely need the following procedure to pull
the changes into the main repository:</p>
<pre><code>see /git/scripts/updatewiki.sh
</code></pre>
<p>This script makes sure the changes are pulled in with the correct permissions.</p>
<h2>Repositories</h2>
<p>This is for moderators.</p>
<p>To create a new repository just git init --bare and symlink the git hooks (see
the hooks/ directory in the other projects).</p>
</div>

</div>

</html>
