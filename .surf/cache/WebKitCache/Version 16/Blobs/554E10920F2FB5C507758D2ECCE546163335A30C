<!doctype html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>surf | suckless.org software that sucks less</title>
	<link rel="stylesheet" type="text/css" href="//suckless.org/pub/style.css"/>
</head>

<div id="header">
	<a href="//suckless.org/"><img src="//suckless.org/logo.svg" alt=""/></a>&nbsp;
	<a id="headerLink" href="//suckless.org/">suckless.org</a>
	<span class="hidden"> - </span>
	<span id="headerSubtitle">surf</span>
</div>
<hr class="hidden"/>
<div id="menu">
	<a href="//suckless.org/">home</a>
	<a href="//dwm.suckless.org/">dwm</a>
	<a href="//st.suckless.org/">st</a>
	<a href="//core.suckless.org/">core</a>
	<a href="//surf.suckless.org/"><b>surf</b></a>
	<a href="//tools.suckless.org/">tools</a>
	<a href="//libs.suckless.org/">libs</a>
	<a href="//ev.suckless.org/">e.V.</a>
	<span class="right">
		<a href="//dl.suckless.org">download</a>
		<a href="//git.suckless.org">source</a>
	</span>
</div>
<hr class="hidden"/>
<div id="content">
<div id="nav">
	<ul>
	<li><a href="/">about</a></li>
	<li><a href="//surf.suckless.org/files/">files/</a></li>
	<li><a href="//surf.suckless.org/patches/"><b>patches/</b></a>
		<ul>
		<li><a href="//surf.suckless.org/patches/aria2/">aria2/</a></li>
		<li><a href="//surf.suckless.org/patches/autoopen/"><b>autoopen/</b></a></li>
		<li><a href="//surf.suckless.org/patches/bookmarking/">bookmarking/</a></li>
		<li><a href="//surf.suckless.org/patches/cachedir/">cachedir/</a></li>
		<li><a href="//surf.suckless.org/patches/chromebar/">chromebar/</a></li>
		<li><a href="//surf.suckless.org/patches/chromekeys/">chromekeys/</a></li>
		<li><a href="//surf.suckless.org/patches/clipboard-instead-of-primary/">clipboard instead of primary/</a></li>
		<li><a href="//surf.suckless.org/patches/dlconsole/">dlconsole/</a></li>
		<li><a href="//surf.suckless.org/patches/download/">download/</a></li>
		<li><a href="//surf.suckless.org/patches/externalpipe/">externalpipe/</a></li>
		<li><a href="//surf.suckless.org/patches/externalpipe-signal/">externalpipe signal/</a></li>
		<li><a href="//surf.suckless.org/patches/history/">history/</a></li>
		<li><a href="//surf.suckless.org/patches/homepage/">homepage/</a></li>
		<li><a href="//surf.suckless.org/patches/instapaper/">instapaper/</a></li>
		<li><a href="//surf.suckless.org/patches/keycodes/">keycodes/</a></li>
		<li><a href="//surf.suckless.org/patches/middle-click-plumb/">middle click plumb/</a></li>
		<li><a href="//surf.suckless.org/patches/modal/">modal/</a></li>
		<li><a href="//surf.suckless.org/patches/multijs/">multijs/</a></li>
		<li><a href="//surf.suckless.org/patches/navigation-history/">navigation history/</a></li>
		<li><a href="//surf.suckless.org/patches/notifications/">notifications/</a></li>
		<li><a href="//surf.suckless.org/patches/omnibar/">omnibar/</a></li>
		<li><a href="//surf.suckless.org/patches/playexternal/">playexternal/</a></li>
		<li><a href="//surf.suckless.org/patches/popup-on-gesture/">popup on gesture/</a></li>
		<li><a href="//surf.suckless.org/patches/proxyconfig/">proxyconfig/</a></li>
		<li><a href="//surf.suckless.org/patches/scroll-faster/">scroll faster/</a></li>
		<li><a href="//surf.suckless.org/patches/searchengines/">searchengines/</a></li>
		<li><a href="//surf.suckless.org/patches/smoothscrolling-via-GTK3/">smoothscrolling via GTK3/</a></li>
		<li><a href="//surf.suckless.org/patches/spacesearch/">spacesearch/</a></li>
		<li><a href="//surf.suckless.org/patches/startgo/">startgo/</a></li>
		<li><a href="//surf.suckless.org/patches/unicode-in-dmenu/">unicode in dmenu/</a></li>
		<li><a href="//surf.suckless.org/patches/url-filtering/">url filtering/</a></li>
		<li><a href="//surf.suckless.org/patches/useragent/">useragent/</a></li>
		<li><a href="//surf.suckless.org/patches/web-search/">web search/</a></li>
		<li><a href="//surf.suckless.org/patches/xdg/">xdg/</a></li>
		</ul>
	</li>
	<li><a href="//surf.suckless.org/screenshots/">screenshots/</a></li>
	<li><a href="//surf.suckless.org/stylesheets/">stylesheets/</a></li>
	</ul>
</div>
<hr class="hidden"/>
<div id="main">

<h1>auto open downloads</h1>
<h2>Description</h2>
<p>This patch uses xdg-open to open a download once it has finished.</p>
<p>It simply replaces this:</p>
<pre><code>&quot;xterm -e \&quot;wget --load-cookies ~/.surf/cookies.txt '$0';\&quot;&quot;, \
</code></pre>
<p>with this:</p>
<pre><code>&quot;ofile=\&quot;$(xdg-user-dir DOWNLOAD)/$(basename \&quot;$0\&quot;)\&quot;; wget --load-cookies ~/.surf/cookies.txt -O \&quot;$ofile\&quot; \&quot;$0\&quot;; xdg-open \&quot;$ofile\&quot;&quot;, \
</code></pre>
<p>in your config.def.h file.</p>
<h2>Download</h2>
<ul>
<li><a href="surf-0.3-autoopen.diff">surf-0.3-autoopen.diff</a> (.5k) (20100705)</li>
</ul>
<h2>Author</h2>
<ul>
<li>Matthew Bauer <a href="&#x6D;&#x61;i&#x6C;&#x74;&#x6F;:&#109;&#106;&#98;&#97;&#117;&#101;&#114;&#57;&#53;&#64;&#103;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;">&#109;&#106;&#98;&#97;&#117;&#101;&#114;&#57;&#53;&#64;&#103;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;</a></li>
</ul>
</div>

</div>

</html>
