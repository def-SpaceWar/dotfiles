import pygame

(WIDTH, HEIGHT) = (800, 600)
win = pygame.display.set_mode((WIDTH, HEIGHT))

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit()
