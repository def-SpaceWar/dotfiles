if !exists('g:vscode')
    set noerrorbells
    set cindent
    set smarttab
    set tabstop=4
    set softtabstop=4
    set shiftwidth=4
    set expandtab
    set smartindent
    set number
    set relativenumber
    set noswapfile
    set nobackup
    set undodir=~/.config/nvim/undodir
    set undofile

    if $TERM =~ '^\(rxvt\|screen\|interix\|putty\)\(-.*\)\?$'
        set notermguicolors
    elseif $TERM =~ '^\(tmux\|iterm\|vte\|gnome\)\(-.*\)\?$'
        set termguicolors
    elseif $TERM =~ "xterm-256color"
        set termguicolors
    elseif $TERM =~ '^\(xterm\)\(-.*\)\?$'
        if $XTERM_VERSION != ''
            set termguicolors
        elseif $KONSOLE_PROFILE_NAME != ''
            set termguicolors
        elseif $VTE_VERSION != ''
            set termguicolors
        else
            set notermguicolors
        endif
    endif

    set laststatus=2
    set hidden
    "set scrolloff=14
    set updatetime=100
    set exrc
    set colorcolumn=80

    highlight ColorColumn guibg=#4D4E4F ctermbg=8

    call plug#begin('~/.config/nvim/plugged')

    Plug 'leafgarland/typescript-vim'
    Plug 'Chiel92/vim-autoformat'
    Plug 'pangloss/vim-javascript'
    Plug 'crusoexia/vim-javascript-lib'
    Plug 'PotatoesMaster/i3-vim-syntax'
    Plug 'vim-python/python-syntax'
    Plug 'chrisbra/Colorizer'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'jackguo380/vim-lsp-cxx-highlight'
    Plug 'habamax/vim-godot'
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
    Plug 'MaxMEllon/vim-jsx-pretty'
    Plug 'mbbill/undotree'
    Plug 'tpope/vim-fugitive'
    Plug 'ThePrimeagen/vim-be-good'
    Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }
    Plug 'ap/vim-buftabline'

    Plug 'nvim-lua/popup.nvim'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'kyazdani42/nvim-tree.lua'
    Plug 'hoob3rt/lualine.nvim'
    Plug 'rktjmp/lush.nvim'

    Plug 'def-SpaceWar/monokai-apprentice.vim'

    call plug#end()

    " Lua configs
    lua require('config')

    " Theme config
    syntax on
    set background=dark
    colorscheme monokai-apprentice
    highlight Normal ctermbg=none guibg=none

    " Plugin config
    "let g:airline_powerline_fonts=1
    "let g:airline_left_sep="\ue0b8"
    "let g:airline_right_sep="\uE0BE"
    let g:coc_global_extensions=[
                \   'coc-json',
                \   'coc-snippets',
                \   'coc-clangd',
                \   'coc-python',
                \   'coc-tsserver',
                \   'coc-styled-components',
                \   'coc-go',
                \   'coc-emmet',
                \]
    let g:lsp_cxx_hl_ure_text_props=1
    let g:python_highlight_all=1
    let g:mkdp_browser='surf'
    let g:zenmode_background = 'dark'
    let g:zenmode_colorscheme = 'monokai-apprentice'
    "let g:zenmode_font ='Cousine 12'

    " Nice keybinds
    let mapleader=' '
    nnoremap <silent> <Leader>u :UndotreeShow<CR>
    nnoremap <silent> <Leader>= :vertical resize +5<CR>
    nnoremap <silent> <Leader>- :vertical resize -5<CR>
    nnoremap <silent> <Leader>+ :resize +5<CR>
    nnoremap <silent> <Leader>_ :resize -5<CR>
    nnoremap <silent> <Leader>bn :bn<CR>
    nnoremap <silent> <Leader>bp :bp<CR>
    nnoremap <silent> <Leader>bd :bd<CR>
    nnoremap <silent> Y y$
    vnoremap <silent> J :m '>+1<CR>gv=gv
    vnoremap <silent> K :m '<-2<CR>gv=gv
    inoremap <silent> <A-j> <esc>:m .+1<CR>==a
    inoremap <silent> <A-k> <esc>:m .-2<CR>==a
    nnoremap <silent> <A-j> <esc>:m .+1<CR>==
    nnoremap <silent> <A-k> <esc>:m .-2<CR>==
    " Zen mode failure
    "nmap <Leader>z :<CR>
    nnoremap <silent> <Leader>t :terminal<CR> iclear<CR>

    " IDE like thingies
    nnoremap <silent> <Leader>cd :call CocActionAsync('jumpDefinition')<CR>
    nnoremap <silent> <Leader>cf :CocFix<CR>
    nnoremap <silent> <Leader>mp :MarkdownPreview<CR>
    nnoremap <silent> <Leader>s  :ColorToggle<CR>
    nnoremap <silent> <F1> :Autoformat<CR>

    " File Exploration/Management
    nnoremap <silent> <Leader>ps :lua require('telescope.builtin').grep_string({search=vim.fn.input("Grep for > ")})<CR>
    nnoremap <silent> <Leader>pf :lua require('telescope.builtin').find_files()<CR>
    nnoremap <silent> <C-p> :lua require('telescope.builtin').git_files()<CR>
    nnoremap <silent> <Leader>pt :NvimTreeToggle<CR>

    " Fugitive
    nmap <silent> <Leader>gs :G<CR>
    nmap <silent> <Leader>gc :Git commit<CR>
    nmap <silent> <Leader>gp :Git push<CR>
    nmap <silent> <Leader>gP :Git pull<CR>
    nmap <silent> <Leader>gf :diffget //3<CR>
    nmap <silent> <Leader>gj :diffget //2<CR>

    " Auto Commands
    augroup AUTO_CMDS
        autocmd!
        autocmd BufWrite *.vim :Autoformat
        autocmd BufWrite *.vim :Autoformat
        autocmd BufWrite *.css :Autoformat
        autocmd BufWrite *.html :Autoformat
        autocmd BufWrite *.c :Autoformat
        autocmd BufWrite *.h :Autoformat
        autocmd BufWrite *.cpp :Autoformat
        autocmd BufWrite *.hpp :Autoformat
    augroup end

    " command Init exec 'e $HOME/.config/nvim/init.vim'
    nmap <silent> <Leader>iv :e /home/aryan/.config/nvim/init.vim<CR>
    nmap <silent> <Leader>cl :e /home/aryan/.config/nvim/lua/config.lua<CR>
    nmap <silent> <Leader>cc :CocConfig<CR>
endif
