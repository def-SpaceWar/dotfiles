# Disable Fish Default Greeting
set fish_greeting

# Shell Color Script
colorscript -e random

# Add emacs PATH
fish_add_path /home/aryan/.emacs.d/bin/

# Starship
starship init fish | source

# DotFiles
alias dotfiles="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"

# Better LS
alias ls="exa -lah"
alias lt="exa -ahT"

# Add Color Script to clear
alias clear="clear && colorscript -e random"

# Kill Mc
alias killmc="killall java && exit"

